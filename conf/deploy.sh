#!/bin/bash
cd /var/www
TODAY=`date +%Y-%m-%d-%H-%M-%S`
DEPLOYDIR="/var/www/deploy/gnipstream-$TODAY"
cp -r /home/ubuntu/gnipstream $DEPLOYDIR
rm current
ln -s $DEPLOYDIR current