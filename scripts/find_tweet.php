<?php

require('../public/config.inc');

/**
 * Read queue, convert into tweet rows
 */

class Awesm_FindTweet_Processor
{
	
	private $db;
	
	public function __construct()
	{
		$this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER,DB_PASS);
	}

	public function process()
	{
		$findSql = "SELECT * FROM queue WHERE processed = 1 ORDER BY created_at ASC LIMIT 500";

		while(true)
		{
			$st = $this->db->prepare($findSql);
			$st->execute();
			$rawTweets = $st->fetchAll();
			
			$processed = array();
			foreach($rawTweets as $rawTweet)
			{
				$processed[] = $rawTweet['id'];
				$tweet = json_decode($rawTweet['body'],true);
				
				//print_r($tweet);
				//$lookingFor = 'StartupPro';
				//$lookingFor = 'twl';
				//$lookingFor = 'sirpa_aggarwal';
				$lookingFor = 'RICCentre';
				
				if ($tweet['actor']['preferredUsername'] == $lookingFor) {
					print_r($tweet);
				}
				
			}

			echo time() . ": Processed " . count($processed) . " tweets\n";

			// mark tweets as processed
			$updateSql = "UPDATE queue SET processed = 2 WHERE id IN (".implode(',',$processed).")";
			$st = $this->db->prepare($updateSql);
			$st->execute();

			sleep(2);
			
		}
		
	}
	
	private function getTweetType($tweet,$awesmId)
	{
		// TODO: cache these lookups
		
		// easiest: if it's a retweet, the object type will tell us
		$objectType = $tweet['object']['objectType'];
		if ($objectType == 'activity')
		{
			// it's a retweet
			// TODO: work out which tweet it's a retweet of, set as parent id
			return array('retweet',null);
		}
		
		// if that's not good enough, look for any original tweets with this $awesmId
		$sql = "SELECT id FROM tweets WHERE awesm_id = ? AND type = 'tweet'";
		$st = $this->db->prepare($sql);
		$st->execute(array($awesmId));
		$mentionOf = $st->fetch();
		
		if (!empty($mentionOf))
		{
			// it's a mention
			return array('mention',$mentionOf['id']);
		}
		
		// no matches: this is an original tweet!
		return array('tweet',null);
	}
	
	private function getAccountByOriginalDomain($domain)
	{
		// TODO: look up & cache domains -> account id mapping here.
		$domainsToAccounts = array(
			'www.feld.com' => 1605,
			'blog.awe.sm' => 3,
			'totally.awe.sm' => 3,
			'hypem.com' => 21,
			'www.bothsidesofthetable.com' => 645
		);
		if (array_key_exists($domain,$domainsToAccounts)) {
			return $domainsToAccounts[$domain];
		}
		return false;
	}
	
	private function isAwesmUrl($url,$accountId)
	{
		$domains = $this->getShortDomainsForAccount($accountId);
		if (empty($domains)) return false;
		$urlParts = parse_url($url);
		if (in_array($urlParts['host'],$domains)) {
			return true;
		}
		return false;
	}
	
	private function getShortDomainsForAccount($accountId)
	{
		// TODO: lookup and & cache account->short domains mapping here
		$accountsToDomains = array(
			3 => array(	// awe.sm
				'awe.sm'
			),
			21 => array( // hype machine
				'awe.sm'
			),
			645 => array(
				'bothsid.es'
			),
			1605 => array(
				'fndry.gr'
			)
		);
		if (array_key_exists($accountId,$accountsToDomains))
		{
			return $accountsToDomains[$accountId];
		}
		return null;
	}
	
}


$processor = new Awesm_FindTweet_Processor();
$processor->process();