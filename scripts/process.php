<?php

require('../public/config.inc');

/**
 * Read queue, convert into tweet rows
 */

class Awesm_PowerTrack_Processor
{
	
	private $db;
	
	public function __construct()
	{
		$this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER,DB_PASS);
	}

	public function process()
	{
		$findSql = "SELECT * FROM queue WHERE processed = 0 ORDER BY created_at ASC LIMIT 100";

		while(true)
		{
			$st = $this->db->prepare($findSql);
			$st->execute();
			$rawTweets = $st->fetchAll();
			
			$processed = array();
			foreach($rawTweets as $rawTweet)
			{
				$processed[] = $rawTweet['id'];
				$tweet = json_decode($rawTweet['body'],true);
				
				$displayUrl = @$tweet['twitter_entities']['urls'][0]['display_url'];
				if (empty($displayUrl)) {
					// twitter couldn't expand the URL for some reason, probably bad link
					continue;
				}
				
				$expandedUrl = $tweet['twitter_entities']['urls'][0]['expanded_url'];
				$awesmId = str_replace('/','_',$displayUrl);
				
				$originalUrl = $tweet['gnip']['urls'][0]['expanded_url'];
				$originalUrlParts = parse_url($originalUrl);
				$originalDomain = $originalUrlParts['host'];
				
				// TODO: work out if this is a retweet etc, set type etc.
				list($type,$parentId) = $this->getTweetType($tweet,$awesmId);
				
				$accountId = $this->getAccountByOriginalDomain($originalDomain);
				
				// skip if this domain is a false positive
				if (!$accountId) continue;
				
				// also skip if this wasn't an awesm url on a valid domain for this account
				if (!$this->isAwesmUrl($expandedUrl, $accountId)) continue;
				
				$reach = $tweet['actor']['followersCount'];
				
				$sql = "INSERT INTO tweets (
							tweet_id,
							type,
							parent_id,
							account_id,
							original_domain,
							awesm_id,
							parent_awesm_id,
							body,
							reach,
							clicks,
							pageviews,
							created
						) VALUES (?,?,?,?,?,?,?,?,?,?,?,now())";
				
				echo "inserting tweet " . $tweet['id'] . "\n";
				echo "domain $originalDomain (account $accountId)\n";
				echo "awesm ID $awesmId from url $displayUrl\n";
				
				$st = $this->db->prepare($sql);
				$st->execute(array(
					$tweet['id'],
					$type,
					$parentId,
					$accountId,
					$originalDomain,
					$awesmId, // awesm_id
					null, // parent_awesm_id
					$rawTweet['body'],
					$reach, // reach
					null, // clicks
					null  // pageviews
				));				
			}

			echo time() . ": Processed " . count($processed) . " tweets\n";

			// mark tweets as processed
			$updateSql = "UPDATE queue SET processed = 1 WHERE id IN (".implode(',',$processed).")";
			$st = $this->db->prepare($updateSql);
			$st->execute();

			sleep(2);
			
		}
		
	}
	
	private function getTweetType($tweet,$awesmId)
	{
		// TODO: cache these lookups
		
		// easiest: if it's a retweet, the object type will tell us
		$objectType = $tweet['object']['objectType'];
		if ($objectType == 'activity')
		{
			// it's a retweet
			// TODO: work out which tweet it's a retweet of, set as parent id
			return array('retweet',null);
		}
		
		// if that's not good enough, look for any original tweets with this $awesmId
		$sql = "SELECT id FROM tweets WHERE awesm_id = ? AND type = 'tweet'";
		$st = $this->db->prepare($sql);
		$st->execute(array($awesmId));
		$mentionOf = $st->fetch();
		
		if (!empty($mentionOf))
		{
			// it's a mention
			return array('mention',$mentionOf['id']);
		}
		
		// no matches: this is an original tweet!
		return array('tweet',null);
	}
	
	private function getAccountByOriginalDomain($domain)
	{
		// TODO: look up & cache domains -> account id mapping here.
		$domainsToAccounts = array(
			'www.feld.com' => 1605,
			'blog.awe.sm' => 3,
			'totally.awe.sm' => 3,
			'hypem.com' => 21,
			'www.bothsidesofthetable.com' => 645
		);
		if (array_key_exists($domain,$domainsToAccounts)) {
			return $domainsToAccounts[$domain];
		}
		return false;
	}
	
	private function isAwesmUrl($url,$accountId)
	{
		$domains = $this->getShortDomainsForAccount($accountId);
		if (empty($domains)) return false;
		$urlParts = parse_url($url);
		if (in_array($urlParts['host'],$domains)) {
			return true;
		}
		return false;
	}
	
	private function getShortDomainsForAccount($accountId)
	{
		// TODO: lookup and & cache account->short domains mapping here
		$accountsToDomains = array(
			3 => array(	// awe.sm
				'awe.sm'
			),
			21 => array( // hype machine
				'awe.sm'
			),
			645 => array(
				'bothsid.es'
			),
			1605 => array(
				'fndry.gr'
			)
		);
		if (array_key_exists($accountId,$accountsToDomains))
		{
			return $accountsToDomains[$accountId];
		}
		return null;
	}
	
}


$processor = new Awesm_PowerTrack_Processor();
$processor->process();