<?php

/**
 * Inhale stream, throw into queue.
 */

require('../public/config.inc');
require('../library/class.gnip_consume.php');

class Awesm_PowerTrack_Consumer extends GnipPowerTrack_Consume
{
	private $db;
	
	public function __construct($username,$password)
	{
		$this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER,DB_PASS);
		parent::__construct($username,$password);
	}
	
	public function enqueue($message)
	{
		$sql = "INSERT INTO queue (body,created_at) VALUES (?,null)";
		$st = $this->db->prepare($sql);
		$st->execute(array($message));
	}
}


// Start streaming
$consumer = new Awesm_PowerTrack_Consumer('awesm', 'str34m1ng');
$consumer->consume();
