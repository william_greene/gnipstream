<?php

// determine environment
$env = getenv('APPLICATION_ENVIRONMENT');
if (!empty($env)) {
	define('APPLICATION_ENVIRONMENT',$env);
} else {
	define('APPLICATION_ENVIRONMENT','production');
}

// env-specific
switch(APPLICATION_ENVIRONMENT)
{
	case 'development':
		define('DB_HOST','localhost');
		define('DB_NAME','gnipstream');
		define('DB_USER','seldo');
		define('DB_PASS','seldo');
		break;
	default:
		define('DB_HOST','localhost');
		define('DB_NAME','gnipstream');
		define('DB_USER','gnipstream');
		define('DB_PASS','str34mr34d3r');
}

// env-agnostic
define('PAGE_SIZE',20);
