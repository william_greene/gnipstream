<?php
/**
 * Get the account id for the api key
 * Find all tweets matching the account ID, build a table of tweets/retweets/mentions.
 */

require('config.inc');
require('display.inc');

$apiKey = @$_GET['api_key'];
$page = @$_GET['page'];
$pageSize = @$_GET['page_size'];

if (empty($page)) $page = 1;
if (empty($pageSize)) $pageSize = PAGE_SIZE;

if (!empty($apiKey)) {
	$tweetMangler = new Awesm_TweetMangler();
	$tweets = $tweetMangler->listTweetsWithMetadata($apiKey,$page,$pageSize);
	$accountName = $tweetMangler->getAccountName($apiKey);
	error_log(print_r($tweets,true));	
}

header ('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>awe.sm stream combination data</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" href="/static/screen.css" />
</head>
<body>

<?php if (!empty($tweets)): ?>
<h1>Tweets for <?= $accountName ?></h1>

	<?php foreach($tweets as $tweet): ?>
<div class="statsContainer clearfix">
	<div class="tweets">
		<div class="tweetMeta clearfix">
			<img class="tweetImage" src="<?= $tweet['image'] ?>" alt="" />
			<a class="tweetAuthor" href="http://twitter.com/<?= $tweet['author_username'] ?>">@<?= $tweet['author_username'] ?></a> <span class="tweetName">(<?= $tweet['author'] ?>)</span>
			<div class="tweetDate"><a href="<?= $tweet['permalink'] ?>" target="_blank"><?= $tweet['date'] ?></a></div>
		</div>
		
		<div class="tweetRow">
			<div class="tweet"><?= $tweet['status_markup'] ?></div>
		<?php if (!empty($tweet['child_tweets'])): ?>
			<div class="tweetReach"><?= number_format($tweet['reach'], 0, '.', ',') ?></div>
		<?php endif; ?>
		</div>
		
		<?php
		if (!empty($tweet['child_tweets'])) {
			print_children($tweet['child_tweets']);
		}
		?>
	</div>
	<div class="stats">
		<table class="totals">
			<thead>
				<tr>
					<th class="statColumn">Reach</th>
					<th class="statColumn">Clicks</th>
					<th class="statColumn">Page Views</th>
					<th class="statColumn"><abbr title="Effective Click Thru Rate">ECTR</abbr></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="statValue"><?= number_format($tweet['total_reach'], 0, '.', ',') ?></td>
					<td class="statValue"><?= number_format($tweet['clicks'], 0, '.', ',') ?></td>
					<td class="statValue"><?= number_format($tweet['pageviews'], 0, '.', ',') ?></td>
					<td class="statValue"><?= sprintf('%01.2f',$tweet['ectr']) ?>%</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
		<?php
		//  $tweet['url'] flip out
		?>
	<?php endforeach; ?>

<div class="pagination">
	<?php if ($page > 1): ?>
		<span class="prev"><a href="?<?= "api_key=$apiKey&pageSize=$pageSize&page=" . ($page-1) ?>">prev</a></span>
	<?php endif; ?>
	<span class="page"><?= $page ?></span>
	<span class="next"><a href="?<?= "api_key=$apiKey&pageSize=$pageSize&page=" . ($page+1) ?>">next</a></span>
</div>

<?php else: ?>
	<?php
	if (!empty($apiKey)) {
		echo "<h1>Sorry, no more tweets for this key</h1>";
	}
	?>
<?php endif; ?>

<hr />

<h3>Find tweets for your account</h3>
<ul>
	<li><a href="?api_key=74d3ed005697a451004d356f2868706255383f4badd19bb64503b515a066aa38">Both Sides of The Table</a></li>
	<li><a href="?api_key=d7f349886b7e76e685cbb2dc4a992c20685df0b5ff63aa4f80ca143485e849ca">Hype Machine</a></li>
	<li><a href="?api_key=9e3ae5ac32a8b6e6e5db9c7c4d765e8e0555f9324cb5762c21fe39632357a87e">Brad Feld</a></li>	
	<li><a href="?api_key=f2d8aeb112f1e0bedd7c05653e3265d2622635a3180f336f73b172267f7fe6ee">awe.sm</a></li>
</ul>

</body>
</html>
<?php
function print_children($tweets) {
	foreach($tweets as $tweet) {
?>
		<div class="retweet">
			<div class="retweetWrap">
				<div class="retweetImage">
					<img class="retweetImage" src="<?= $tweet['image'] ?>" alt="" />
				</div>
				<div class="retweetTweet">
					<a href="http://twitter.com/<?= $tweet['author_username'] ?>">@<?= $tweet['author_username'] ?></a>: 
					<?= $tweet['status_markup'] ?>
				</div>
				<div class="retweetDate">
					<a href="<?= $tweet['permalink'] ?>" target="_blank"><?= $tweet['date'] ?></a>
				</div>
			</div>
			<div class="tweetReach"><?= number_format($tweet['reach'], 0, '.', ',') ?></div>
		</div>
		<?php
	}
}