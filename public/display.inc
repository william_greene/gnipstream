<?php

class Awesm_TweetMangler
{
	
	private $db;
	const CACHE_PERIOD = 300; // five minutes
	
	// fugly
	public function __construct()
	{
		$this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER,DB_PASS);
	}	
	
	public function listTweetsWithMetadata($apiKey,$page,$pageSize)
	{
		$accountId = $this->getAccountIdByApiKey($apiKey);
		
		if (empty($accountId)) throw new Exception("Account ID not found");
		if (empty($page)) $page = 1;
		if (empty($pageSize)) $pageSize = PAGE_SIZE;
		
		$offset = $pageSize*($page-1);
		
		// find original tweets
		$sql = "SELECT * FROM tweets 
				WHERE account_id = $accountId 
				AND type = 'tweet' 
				ORDER BY created DESC LIMIT $offset,$pageSize";
		error_log($sql);
		$st = $this->db->prepare($sql);
		$st->execute();
		$rawTweets = $st->fetchAll();
		
		$tweets = array();
		foreach($rawTweets as $rawTweet)
		{
			
			$tweetData = $this->refreshMetadata($apiKey,$rawTweet);
			
			$childTweets = $this->getChildren($apiKey,$rawTweet);
			
			//error_log(print_r($childTweets,true));
			
			// calculate mention count
			$totalMentions = count($childTweets) + 1;
			
			// calculate total reach
			$totalReach = $rawTweet['reach'];
			foreach($childTweets as $ct) {
				$totalReach += $ct['reach'];
			}
			
			// assemble the required fields for the output
			$tweet = $this->formatTweet($tweetData);
			if (!empty($childTweets)) {
				$tweet['child_tweets'] = $childTweets;
			}
			$tweet['total_mentions'] = $totalMentions;
			$tweet['total_reach'] = $totalReach;
			$tweet['ectr'] = round(100*($tweet['clicks']/$tweet['total_reach']),2);
			
			$tweets[] = $tweet;
			
		}
		
		return $tweets;		
	}
	
	public function getAccountName($apiKey)
	{
		// TODO: look this up for realz
		$apiKeysToAccountNames = array(
			'74d3ed005697a451004d356f2868706255383f4badd19bb64503b515a066aa38' => "Both Sides of The Table",
			'd7f349886b7e76e685cbb2dc4a992c20685df0b5ff63aa4f80ca143485e849ca' => "Hype Machine",
			'9e3ae5ac32a8b6e6e5db9c7c4d765e8e0555f9324cb5762c21fe39632357a87e' => "Brad Feld",
			'f2d8aeb112f1e0bedd7c05653e3265d2622635a3180f336f73b172267f7fe6ee' => "awe.sm"
		);
		return $apiKeysToAccountNames[$apiKey];
	}

	/**
	 * Arrange fields as the output is expecting.
	 * @param type $tweetData
	 * @return type 
	 */
	private function formatTweet($tweetData)
	{
		$tcoUrl = $tweetData['streamdata']['twitter_entities']['urls'][0]['url'];
		$awesmUrl = $tweetData['streamdata']['twitter_entities']['urls'][0]['expanded_url'];
		$displayUrl = $tweetData['streamdata']['twitter_entities']['urls'][0]['display_url'];
		$status = $tweetData['streamdata']['body'];
		$awesmLink = '<a href="'.$awesmUrl.'" target="_blank">'.$displayUrl.'</a>';
		$statusMarkup = str_replace($tcoUrl,$awesmLink,$status);
		
		return array(
			'author' => $tweetData['streamdata']['actor']['displayName'],
			'author_username' => $tweetData['streamdata']['actor']['preferredUsername'],
			'status' => $status,
			'status_markup' => $statusMarkup,
			'permalink' => $tweetData['streamdata']['link'],
			'date' => $this->formatRelativeTime($tweetData['streamdata']['postedTime']),
			'url' => $awesmUrl,
			'reach' => $tweetData['reach'],
			'clicks' => $tweetData['clicks'],
			'pageviews' => $tweetData['pageviews'],
			'type' => $tweetData['type'],
			'image' => $tweetData['streamdata']['actor']['image']
		);		
	}
	
	/**
	 * Find child tweets of this tweet and format them
	 * @param type $rawTweet
	 * @return type 
	 */
	private function getChildren($apiKey,$rawTweet)
	{
		$sql = "SELECT * FROM tweets WHERE awesm_id = ? AND type != 'tweet' ORDER BY created DESC LIMIT 50";
		$st = $this->db->prepare($sql);
		$st->execute(array($rawTweet['awesm_id']));
		$rawChildTweets = $st->fetchAll();
		
		$childTweets = array();
		foreach($rawChildTweets as $rawChildTweet) {
			$tweetData = $this->refreshMetadata($apiKey,$rawChildTweet);
			$childTweets[] = $this->formatTweet($tweetData);
		}
		
		return $childTweets;
	}
	
	/**
	 * Returns tweet metadata, updates it if necessary
	 * TODO: could format the tweet in here as well.
	 * @param type $rawTweet 
	 */
	private function refreshMetadata($apiKey,$rawTweet)
	{
		// fetch awe.sm stats if not fetched recently
		$updated = strtotime($rawTweet['updated']);
		$metadata = false;
		if ( $updated < time() - self::CACHE_PERIOD || empty($rawTweet['awesm_metadata']) )
		{
			$url = "http://api.awe.sm/stats/" . $rawTweet['awesm_id'] . ".json?v=3&key=" . $apiKey . "&with_metadata=true&with_conversions=true";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			$response = curl_exec($ch);
			$metadata = json_decode($response,true);
			$rawTweet['clicks'] = $metadata['clicks'];
			$rawTweet['pageviews'] = $metadata['conversions']['pageviews'];

			// cache fields and touch the updated
			$sql = "UPDATE tweets SET 
						awesm_metadata = ?, 
						clicks = ?,
						pageviews = ?,
						updated = now() 
					WHERE id = ? LIMIT 1";
			$st = $this->db->prepare($sql);
			$st->execute(array(
				$response,
				$rawTweet['clicks'],
				$rawTweet['pageviews'],
				$rawTweet['id']
			));
		}
		else {
			$metadata = json_decode($rawTweet['awesm_metadata'],true);
		}

		$tweet = $rawTweet;
		$tweet['metadata'] = $metadata;
		$tweet['streamdata'] = json_decode($rawTweet['body'],true);
		
		return $tweet;
	}
	
	private function getAccountIdByApiKey($apiKey)
	{
		// TODO: lookup and cache keys to account IDs
		$apiKeysToAccounts = array(
			'74d3ed005697a451004d356f2868706255383f4badd19bb64503b515a066aa38' => 645,
			'd7f349886b7e76e685cbb2dc4a992c20685df0b5ff63aa4f80ca143485e849ca' => 21,
			'9e3ae5ac32a8b6e6e5db9c7c4d765e8e0555f9324cb5762c21fe39632357a87e' => 1605,
			'f2d8aeb112f1e0bedd7c05653e3265d2622635a3180f336f73b172267f7fe6ee' => 3
		);
		return $apiKeysToAccounts[$apiKey];
	}


	private function formatPlural($num) {
		if ($num != 1) {
			return "s";
		}
	}
	
	private function formatRelativeTime($date) {
		$diff = time() - strtotime($date);
		
		if ($diff < 60)
			return $diff . " second" . $this->formatPlural($diff) . " ago";
		
		$diff = round($diff/60);
		
		if ($diff<60)
			return $diff . " minute" . $this->formatPlural($diff) . " ago";
		
		$diff = round($diff/60);
		
		if ($diff<24)
			return $diff . " hour" . $this->formatPlural($diff) . " ago";
		
		$diff = round($diff/24);
		
		if ($diff<7)
			return $diff . " day" . $this->formatPlural($diff) . " ago";
		
		return "on " . date("F j, Y", strtotime($date));
	}

}