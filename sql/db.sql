CREATE TABLE `tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet_id` varchar(500) DEFAULT NULL,
  `type` enum('tweet','retweet','mention') DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `original_domain` varchar(500) DEFAULT NULL,
  `awesm_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `parent_awesm_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `body` text,
  `reach` varchar(45) DEFAULT NULL,
  `clicks` varchar(45) DEFAULT NULL,
  `pageviews` varchar(45) DEFAULT NULL,
  `awesm_metadata` text,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=875 DEFAULT CHARSET=utf8;

CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` text NOT NULL,
  `processed` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1146 DEFAULT CHARSET=utf8;